----- Grading -----
[/50] TOTAL
	[/15] q1-4
	[/27] q5
	[/08] q6
	[-05] repo is not private
	[-05] missing shasums NOTE: 0 will be given for answers that do not include the command(s)
--------------------

Do not include name or student ID.

Submit via your handin repository on gitlab before the deadline.
Ensure that you have submitted the repository's URL via the d2l quiz.

For all of these questions, you may NOT use:
	grep, sed, awk, or perl
	if, for, while, until, case, or read


Questions #4 onwards end with some digits in square brackets.  Those digits represent the starting digits of the shasum of the correct output.  For those questions, provide BOTH the command, and the resulting shasum of the output as in question 0 below.

q00) [0] Give a command to replace all of the capital X characters with lower case x characters in the books corpus. [e]

	cat * | tr 'X' 'x' | shasum
	e424cc5e4fae61474f7a7622b2daeb2340999602  -

--------------------


q01) [4] Suppose there are multiple copies of the program "foo" on your machine: one in the current directory, one in your home directory, one in /usr/bin, and one in /bin.

	1) Without any additional information, which one will run if you simply type "foo" on the command line?
	
	bin

	2) Give the command to run the one in the current directory

	./foo

	3) Give the command to run the one in your home directory
	
	/home/foo

	4) Give the command to run the one in /usr/bin
	
	/usr/bin/foo


q02) [3] For each of the following commands, state:
	i) which program opens and reads the input file, and
	ii) how shasum receives the file's contents.

	a)	shasum Meditations.txt
		i) shasum
		ii) parameter
	b)	cat Meditations.txt | shasum
		i) cat
		ii) stdin
	c) shasum < Meditations.txt
		i) shasum
		ii) stdin


q03) [4] give a command to display the reverse ordering of the elements in the path variable (colon delimited). eg: "1a:2b:3c:4d" -> "4d:3c:2b:1a".

	echo $PATH | tr ':' '\n' | tac    

q04) [4] give a set of commands that will create a file "foo" that contains 128 'y' characters followed by 128 'n' characters.  The file should contain only those 256 characters. [6]

	echo $(yes "y" | head -n 128) $(yes "n" | head -n 128) >> output.txt

q05) [27] for the purpose of this question a "word" is defined to be a contiguous sequence of alpha-numeric characters. Answer all questions w/r/t the books corpus (wget http://mylinux.langara.ca/~jhilliker/1280/books.tar.bz2 ; tar -xf books.tar.bz2). Give a command to:

	a) [1] remove all blank lines. "Blank," for this question, means "doesn't contain any characters." [06] or [f9]

	

	b) [2] remove the last character of every line of the books corpus. [d]

		

	c) [2] determine how often the letter 'u' or 'U' appear in the books corpus. [1]

		cat books/* | grep -i "u" | wc -l

	d) [2] display the total word count (number of words, including duplicates). Display only this number. eg: 'he said she said' -> 4 [c]

		cat books/* | wc -w

	e) [4] display the number of unique words (the word count, excluding duplicates), case insensitive (use tr and posix classes). eg: 'he SAID she said' -> 3 [4]

		cat books/* | tr '[:upper:]' '[:lower:]' | tr -cs 'a-zA-Z0-9' '\n' | sort | uniq | wc -w

	f) [4] Display the number of words that occur only once, case insensitive. [a]

		cat books/* | tr '[:upper:]' '[:lower:]' | tr -cs 'a-zA-Z0-9' '\n' | sort | uniq -c | grep 1 | wc -l

	g) [4] display the name of the file in the books corpus that contains the fewest newline characters. [0]

		

	h) [8] display the 10 most frequently occurring words (case insensitive) in descending order (most frequent first), one per line. Display only those words (no counts). Display them in lower case. [a]

		cat books/* | tr '[:upper:]' '[:lower:]' | tr -cs 'a-zA-Z0-9' '\n' | sort | uniq -c | sort -n | tail -10

q06) [8] w/r/t http://mylinux.langara.ca/~jhilliker/1280/pokemon.csv

	a) [7] give a command to display the most common type of pokemon (fields 3&4). If a pokemon has 2 types, count both. [b]
		eg: lines 5&6 would be counted as: 2 Fire, 1 Flying.

		cat pokemon.csv | cut -d ',' -f3,4 | sort | tr ',' '\n'| grep -v -e '^$' | sort | uniq -c | sort | tail -1

	b) [1] how often does that type of pokemon occur? [6]

		126 Water types
